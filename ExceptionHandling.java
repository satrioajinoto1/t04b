import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");
        boolean validInput = false;
        do {
            //edit here
            try {
                System.out.println("Pembilang : ");
                int pembilang = scanner.nextInt();
                System.out.println("Penyebut : ");
                int penyebut = scanner.nextInt();
                System.out.println(pembagian(pembilang, penyebut));
                validInput = true;             
            } catch (InputMismatchException e) {
                System.out.println("Bilangan yang diinput ada yang salah");
                scanner.nextLine();
            } catch (ArithmeticException e){
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0  
        if(penyebut == 0 ){
            throw new ArithmeticException("Penyebut bernilai 0");
        }
        return pembilang / penyebut;
    }
}
